TODO List
========

- [x] Webpack config
- [x] React config
- [x] Redux config
- [x] Material UI components

##Sections
- [ ] Favorites

##Modules
- Favorites
    - [x] Actions
    - [x] Actions tests
    - [x] Reducers

##Componenents
- [x] GameCard
- [x] GamesList
- [x] FavoritesList
- [x] ListFilter

##Nice to have
- [x] Docker
- [ ] API

