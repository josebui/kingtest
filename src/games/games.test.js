import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'

import * as actions from './games.actions'


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Games actions', () => {
  it('Should dispatch actions when getGames()', () => {
    const expectedActions = [
      actions.gamesListRequest(),
      actions.gamesListSuccess(),
    ]
    const store = mockStore({})
    return store.dispatch(actions.getGames())
      .then(() => {
        expect(
          store.getActions().map(action => action.type)
        ).toEqual(
          expectedActions.map(action => action.type)
        )
      })
  })
})
