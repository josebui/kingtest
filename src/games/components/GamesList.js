import React from 'react'
import { connect } from 'react-redux'
import {
  List,
  Avatar,
  ListItem,
  ListItemText,
} from '@material-ui/core'

import NoResult from '../../components/NoResult'
import * as actions from '../games.actions'


// Wait for user typing before sending request
const WAIT_INTERVAL = 500;

class GamesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timer: null,
    }
  }

  componentDidMount() {
    const { getGames, search } = this.props
    getGames(search)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.search !== this.props.search) {
      if (this.state.timer) {
        clearTimeout(this.state.timer)
      }
      this.setState({
        timer: setTimeout(() => {
          this.props.getGames(this.props.search)
        }, WAIT_INTERVAL),
      })
    }
  }

  render() {
    const onGameSelect = game => () => {
      this.props.onSelect(game)
    }

    const filterExcluded = () => {
      const toExclude = this.props.exclude
      return this.props.list.filter(game => toExclude.indexOf(game.short) === -1)
    }

    const gamesToShow = filterExcluded()

    return (
      <div>
        <List>
          {!this.props.isLoading && this.props.list && gamesToShow.length === 0
            ? (<NoResult message="No games found, try a different search." />)
            : null
          }
          {gamesToShow.map(game => (
            <ListItem key={game.short} onClick={onGameSelect(game)} button>
              <Avatar>
                <img
                  src={`http://royal1.midasplayer.com/images/games/${game.short}/${game.short}_60x60.gif`}
                  alt={game.name} />
              </Avatar>
              <ListItemText primary={game.name} secondary={game.short} />
            </ListItem>
          ))}
        </List>
      </div>
    )
  }
}

export default connect(state => ({
  list: state.games.list,
  isLoading: state.games.isLoading,
}), dispatch => ({
  getGames: search => dispatch(actions.getGames(search)),
}))(GamesList)
