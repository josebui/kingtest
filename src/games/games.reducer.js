import { handleActions } from 'redux-actions'

import * as actions from './games.actions'


const initialState = {
  list: [],
  isLoading: false,
}

export default handleActions({
  [actions.gamesListRequest]: state => ({ ...state, isLoading: true }),
  [actions.gamesListSuccess]: (state, action) => ({
    ...state,
    list: action.payload,
    isLoading: false,
  }),
}, initialState)
