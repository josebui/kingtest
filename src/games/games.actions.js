import { createAction } from 'redux-actions'

import * as service from './games.service'


export const gamesListRequest = createAction('GAMES_LIST_REQUEST')
export const gamesListSuccess = createAction('GAMES_LIST_SUCCESS')

export const getGames = search => dispatch => {
  dispatch(gamesListRequest())
  return service.getGames(search)
    .then(games => dispatch(gamesListSuccess(games)))
}
