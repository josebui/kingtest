import q from 'q'

import { findRankedMatches } from '../utils'

import games from './games.json'


/*
 * Mock service for Games
 */
const MOCK_DELAY = 200

// eslint-disable-next-line import/prefer-default-export
export const getGames = search => {
  const deferred = q.defer()
  setTimeout(() => {
    const result = search
      ? findRankedMatches(games.games, 'name', search)
      : games.games
    deferred.resolve(result)
  }, MOCK_DELAY)
  return deferred.promise
}
