import React from 'react'
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  LinearProgress,
} from '@material-ui/core'
import {
  Add,
} from '@material-ui/icons'
import { connect } from 'react-redux'

import Favorites from '../favorites/components/Favorites'
import AddFavoriteDialog from '../favorites/components/AddFavoriteDialog'

import styles from './styles.css'


class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      dialogOpened: false,
    }
  }

  openFavoriteDialog() {
    this.setState({ dialogOpened: true })
  }

  closeFavoriteDialog() {
    this.setState({ dialogOpened: false })
  }

  render() {
    const showLoader = () => {
      if (!this.props.isLoading) {
        return null
      }
      return (
        <LinearProgress color="secondary" />
      )
    }
    return (
      <div>
        <AppBar position="sticky">
          <Toolbar>
            <Typography
              variant="title"
              color="inherit"
              className={styles.appBarTitle}>
            </Typography>
            <Button
              color="inherit"
              onClick={this.openFavoriteDialog.bind(this)}
            >
              <Add />
              <span> Add to Favorite</span>
            </Button>
          </Toolbar>
          <div className={styles.loaderContainer}>
            {showLoader()}
          </div>
        </AppBar>
        <div className={styles.content}>
          <Favorites />
        </div>
        <AddFavoriteDialog
          open={this.state.dialogOpened}
          onClose={this.closeFavoriteDialog.bind(this)}
        />
      </div>
    )
  }
}

export default connect(state => ({
  isLoading: (state.favorites.isLoading || state.favorites.isRemoving || state.favorites.isAdding),
}))(App)
