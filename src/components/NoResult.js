import React from 'react'
import {
  Paper,
  Typography,
  Divider,
} from '@material-ui/core'

import styles from './styles.css'


const NoResult = props => (
  <Paper elevation={0} className={styles.noResult}>
    <Typography component="p" >
      {props.message}
    </Typography>
    <Divider light={true} style={{ margin: '10px 0' }} />
    <img width="300" src="https://www.qrs.in/frontent/images/noresult.png" />
  </Paper>
)

export default NoResult
