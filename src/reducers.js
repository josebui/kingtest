import { combineReducers } from 'redux'

import favorites from './favorites/favorites.reducer'
import games from './games/games.reducer'


export default combineReducers({
  favorites,
  games,
})
