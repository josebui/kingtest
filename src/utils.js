import similarity from 'string-similarity'

// eslint-disable-next-line import/prefer-default-export
export const findRankedMatches = (list, param, search) => {
  if (!list || list.length === 0) {
    return list
  }
  const matches = similarity
    .findBestMatch(
      search.trim(),
      list.map(entry => entry[param])
    )
    .ratings
    .sort((a, b) => b.rating - a.rating)
    .filter(match => match.rating > 0.2)
    .map(match => match.target)

  const results = matches
    .map(match => list.find(entry => entry[param] === match))
  return results
}
