import React from 'react'
import {
  GridList,
  TextField,
  Paper,
} from '@material-ui/core'
import {
  Search,
} from '@material-ui/icons'
import { connect } from 'react-redux'

import NoResult from '../../components/NoResult'
import * as actions from '../favorites.actions'
import FavoriteTile from './FavoriteTile'

import styles from './styles.css'


// Wait for user typing before sending request
const WAIT_INTERVAL = 500;

class Favorites extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      timer: null,
      search: null,
    }
  }

  componentDidMount() {
    this.props.getFavorites()
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.search !== this.state.search) {
      if (this.state.timer) {
        clearTimeout(this.state.timer)
      }
      this.setState({
        timer: setTimeout(() => {
          this.props.getFavorites(this.state.search)
        }, WAIT_INTERVAL),
      })
    }
  }

  onSearchChange(event) {
    this.setState({
      search: event.target.value,
    })
  }

  render() {
    const { props, state } = this
    return (
      <div>
        <Paper className={styles.searchContainer}>
          <TextField
            autoFocus
            margin="dense"
            label={<div className={styles.searchLabel}><Search />Search</div>}
            fullWidth
            onChange={this.onSearchChange.bind(this)}
          />
        </Paper>
        <GridList cellHeight={180} cols={3} className={styles.favoritesGrid}>
          {!props.isLoading && props.list && props.list.length === 0
            ? (<NoResult message={ state.search
              ? 'No favorites found, try a different search.'
              : 'Add more favorites from the top menu.'
            } />)
            : null
          }
          {props.list.map(favorite => (
            <FavoriteTile
              key={favorite.short}
              favorite={favorite} />
          ))}
        </GridList>
      </div>
    )
  }
}

export default connect(state => ({
  list: state.favorites.list,
}), dispatch => ({
  getFavorites: search => dispatch(actions.getFavorites(search)),
}))(Favorites)
