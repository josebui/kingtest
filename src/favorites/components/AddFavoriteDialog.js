import React from 'react'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  LinearProgress,
} from '@material-ui/core'
import {
  Search,
} from '@material-ui/icons'
import { connect } from 'react-redux'

import * as actions from '../favorites.actions'
import GamesList from '../../games/components/GamesList'

import styles from './styles.css'


class AddFavoriteDialog extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchQuery: null,
    }
  }

  onSearchChange(event) {
    this.setState({
      searchQuery: event.target.value,
    })
  }

  render() {
    const { props } = this

    const onGameSelect = game => {
      if (props.isAdding) {
        return
      }
      props.addFavorite(game)
    }

    const showLoader = () => {
      if (!props.isAdding && !props.isLoadingGames) {
        return null
      }
      return (
        <LinearProgress color="secondary" />
      )
    }

    return (
      <div>
        <Dialog
          aria-labelledby="simple-dialog-title"
          open={props.open}
          onClose={props.onClose}
        >
          <div className={styles.loaderContainer}>
            {showLoader()}
          </div>
          <DialogTitle id="simple-dialog-title">
            <span>Add game to favorites</span>
            <TextField
              autoFocus
              margin="dense"
              label={<div className={styles.searchLabel}><Search />Search</div>}
              fullWidth
              onChange={this.onSearchChange.bind(this)}
              value={this.state.searchQuery || ''}
            />
          </DialogTitle>
          <DialogContent>
            <GamesList
              search={this.state.searchQuery}
              onSelect={onGameSelect}
              exclude={this.props.currentFavorites.map(game => game.short)}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={props.onClose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default connect(state => ({
  currentFavorites: state.favorites.list,
  isAdding: state.favorites.isAdding,
  isLoadingGames: state.games.isLoading,
}), dispatch => ({
  addFavorite: game => dispatch(actions.addFavorite(game)),
}))(AddFavoriteDialog)
