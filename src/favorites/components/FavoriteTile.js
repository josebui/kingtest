import React from 'react'
import {
  GridListTile,
  GridListTileBar,
  IconButton,
  Tooltip,
  Button,
} from '@material-ui/core'
import {
  DeleteForever,
  PlayArrow,
} from '@material-ui/icons';
import { connect } from 'react-redux'

import * as actions from '../favorites.actions'

import styles from './styles.css'


const FavoriteTile = props => {
  const { favorite } = props

  const onRemoveClick = () => {
    props.removeFavorite(favorite)
  }

  return (
    <GridListTile
      cols={1}
      key={favorite.short}
      className={styles.tile}
    >
      <div className={styles.deleteButton}>
        <Tooltip title="Remove favorite">
          <IconButton
            onClick={onRemoveClick}
            disabled={props.isRemoving}
          >
            <DeleteForever className={styles.tileActionIcon} />
          </IconButton>
        </Tooltip>
      </div>
      <div>
        <img
          src={`http://royal1.midasplayer.com/images/games/${favorite.short}/tournamentPage/${favorite.short}_764x260.jpg`}
          alt={favorite.name} />
      </div>
      <GridListTileBar
        title={
          <div className={styles.tileBar}>
            <span className={styles.tileBarTitle}>{favorite.name}</span>
            <Button color="secondary" href={`https://king.com/game/${favorite.short}`}>
              <PlayArrow />
              Play
            </Button>
          </div>
        }
        actionPosition="left"
      />
    </GridListTile>
  )
}

export default connect(() => ({
}), dispatch => ({
  removeFavorite: favorite => dispatch(actions.removeFavorite(favorite)),
}))(FavoriteTile)
