import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'

import * as actions from './favorites.actions'


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('Favorites actions', () => {
  it('Should dispatch actions when getFavorites()', () => {
    const expectedActions = [
      actions.favoritesListRequest(),
      actions.favoritesListSuccess(),
    ]
    const store = mockStore({})
    return store.dispatch(actions.getFavorites())
      .then(() => {
        expect(
          store.getActions().map(action => action.type)
        ).toEqual(
          expectedActions.map(action => action.type)
        )
      })
  })
  it('Should dispatch actions when addFavorite()', () => {
    const expectedActions = [
      actions.favoritesAddRequest(),
      actions.favoritesAddSuccess(),
      actions.favoritesListRequest(),
      actions.favoritesListSuccess(),
    ]
    const store = mockStore({})
    return store.dispatch(actions.addFavorite({}))
      .then(() => {
        expect(
          store.getActions().map(action => action.type)
        ).toEqual(
          expectedActions.map(action => action.type)
        )
      })
  })
  it('Should dispatch actions when removeFavorite()', () => {
    const expectedActions = [
      actions.favoritesRemoveRequest(),
      actions.favoritesRemoveSuccess(),
      actions.favoritesListRequest(),
      actions.favoritesListSuccess(),
    ]
    const store = mockStore({})
    return store.dispatch(actions.removeFavorite('test-id'))
      .then(() => {
        expect(
          store.getActions().map(action => action.type)
        ).toEqual(
          expectedActions.map(action => action.type)
        )
      })
  })
})
