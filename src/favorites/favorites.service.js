import q from 'q'

import { findRankedMatches } from '../utils'


/*
 * Mock service for Favorites
 */
const MOCK_DELAY = 200
let favorites = []

const favoriteExist = favorite => {
  const foundFavorite = favorites.find(game => game.short === favorite.short)
  return !!foundFavorite
}

export const getFavorites = search => {
  const deferred = q.defer()
  setTimeout(() => {
    const result = search
      ? findRankedMatches(favorites, 'name', search)
      : favorites
    deferred.resolve(result)
  }, MOCK_DELAY)
  return deferred.promise
}

export const addFavorite = game => {
  const deferred = q.defer()
  const favorite = { ...game }
  setTimeout(() => {
    if (favoriteExist(favorite)) {
      // Game already added
      return deferred.resolve()
    }
    favorites = [favorite, ...favorites]
    return deferred.resolve()
  }, MOCK_DELAY)
  return deferred.promise
}

export const removeFavorite = favorite => {
  const deferred = q.defer()
  setTimeout(() => {
    favorites = favorites.filter(game => game.short !== favorite.short)
    deferred.resolve()
  }, MOCK_DELAY)
  return deferred.promise
}
