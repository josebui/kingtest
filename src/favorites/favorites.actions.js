import { createAction } from 'redux-actions'

import * as service from './favorites.service'


export const favoritesListRequest = createAction('FAVORITES_LIST_REQUEST')
export const favoritesListSuccess = createAction('FAVORITES_LIST_SUCCESS')
export const favoritesAddRequest = createAction('FAVORITE_ADD_REQUEST')
export const favoritesAddSuccess = createAction('FAVORITE_ADD_SUCCESS')
export const favoritesRemoveRequest = createAction('FAVORITE_REMOVE_REQUEST')
export const favoritesRemoveSuccess = createAction('FAVORITE_REMOVE_SUCCESS')


export const getFavorites = search => dispatch => {
  dispatch(favoritesListRequest())
  return service.getFavorites(search)
    .then(favorites => dispatch(favoritesListSuccess(favorites)))
}

export const addFavorite = game => dispatch => {
  dispatch(favoritesAddRequest())
  return service.addFavorite(game)
    .then(() => dispatch(favoritesAddSuccess()))
    .then(() => dispatch(getFavorites()))
}

export const removeFavorite = game => dispatch => {
  dispatch(favoritesRemoveRequest(game))
  return service.removeFavorite(game)
    .then(() => dispatch(favoritesRemoveSuccess()))
    .then(() => dispatch(getFavorites()))
}
