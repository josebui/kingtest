import { handleActions } from 'redux-actions'

import * as actions from './favorites.actions'


const initialState = {
  list: [],
  isLoading: false,
  isAdding: false,
  isRemoving: false,
}

export default handleActions({
  [actions.favoritesListRequest]: state => (
    { ...state, isLoading: true }
  ),
  [actions.favoritesListSuccess]: (state, action) => (
    { ...state, list: action.payload, isLoading: false }
  ),
  [actions.favoritesAddRequest]: state => (
    { ...state, isAdding: true }
  ),
  [actions.favoritesAddSuccess]: state => (
    { ...state, isAdding: false }
  ),
  [actions.favoritesRemoveRequest]: state => (
    { ...state, isRemoving: true }
  ),
  [actions.favoritesRemoveSuccess]: state => (
    { ...state, isRemoving: false }
  ),
}, initialState)
